const score = document.querySelector('.score'),
  start = document.querySelector('.start'),
  gameArea = document.querySelector('.gameArea'),
  car = document.createElement('div');
car.classList.add('car');

start.addEventListener('click', startGame);
document.addEventListener('keydown', startRun);
document.addEventListener('keyup', stopRun);

const keys = {
  ArrowUp: false,
  ArrowDown: false,
  ArrowRight: false,
  ArrowLeft: false,
};

const settings = {
  start: false,
  score: 0,
  speed: 4,
  traffic: 3,
}

/**
* Считаем сколько можно поместить элементов в текущую высоту дороги
*/
function getQuantityElements(elementHeight) {
  return document.querySelector('.game').clientHeight / elementHeight + 1;
}

function startGame() {
  // Прячем кнопку запуска
  start.classList.add('hide');
  // Очищаем игровое поле
  gameArea.innerHTML = '';
  score.style.top = '0px';
  // создаем линии разметки
  for (let i=0; i<getQuantityElements(100); i++) {
    const line = document.createElement('div');
    line.classList.add('line');
    line.style.top = (i * 100) + 'px';
    line.y = i * 100;   // высота линии + смещение по вертикали
    gameArea.appendChild(line);
  }

  // Начальные установки
  settings.score = 0;
  settings.start = true;

  // Добавляем наш автомобиль на поле
  gameArea.appendChild(car);
  // Ставим наш автомобиль в начальную позицию
  car.style.left = '125px';
  car.style.top = 'auto';
  car.style.bottom = '10px';

  settings.x = car.offsetLeft;
  settings.y = car.offsetTop;

  // создаем другие автомобили
  for (let i=0; i<getQuantityElements(100 * settings.traffic); i++) {
    const enemy = document.createElement('div');
    enemy.classList.add('enemy');
    // прячем автомобили наверх, для того чтобы они плавно появлялись сверху
    enemy.y = -100 * settings.traffic * (i + 1);
    // случайная позиция по горизонтали
    enemy.style.left = Math.floor(Math.random() * (gameArea.offsetWidth - car.offsetWidth)) + 'px';
    enemy.style.top = enemy.y + 'px';
    enemy.style.background = "transparent url('./image/enemy.png') center / cover no-repeat";
    gameArea.appendChild(enemy);
  }

  requestAnimationFrame(playGame);
}

/**
* Движение полос разметки
*/
function moveRoad() {
  let lines = document.querySelectorAll('.line');
  lines.forEach((line, i) => {
    // Перемещаем вниз линию разметки путем увеличения свойства стиля top
    line.y += settings.speed;
    line.style.top = line.y + 'px';
    // Если линия ушла вниз за контейнер, перемещаем ее наверх
    if (line.y >= document.querySelector('.game').clientHeight) {
      line.y = -100;
    }
  });
}

/**
* Движение автомобилей Enemy
*/
function moveEnemy() {
  let enemies = document.querySelectorAll('.enemy');
  enemies.forEach((enemy, i) => {
    // Получаем параметры автомобилей
    let carRect = car.getBoundingClientRect();
    let enemyRect = enemy.getBoundingClientRect();

    // Проверяем на столкновения
    if(carRect.top <= enemyRect.bottom &&
      carRect.right >= enemyRect.left &&
      carRect.left <= enemyRect.right &&
      carRect.bottom >= enemyRect.top) {
        // При столкновении останавливаем игру
        settings.start = false;
        // Показываем кнопку запуска путем удаления прячущего класса
        start.classList.remove('hide');
        // Опускаем score чтобы были видны очки
        score.style.top = start.offsetHeight + 'px';
    }
    // Перемещаем автомобиль путем увеличения свойства стиля top
    enemy.y += Math.floor(settings.speed);
    enemy.style.top = enemy.y + 'px';

    // Если автомобиль уехал вниз за контейнер, то возвращаем его наверх
    // и задаем другую случайную позицию по горизонтали
    if (enemy.y >= document.querySelector('.game').clientHeight) {
      enemy.y = -100 * settings.traffic;
      enemy.style.left = Math.floor(Math.random() * (gameArea.offsetWidth - car.clientWidth)) + 'px';
    }
  });
}

/**
* Главный цикл
*/
function playGame() {

  if (settings.start) {
    // Выводим очки
    settings.score += settings.speed;
    score.innerHTML = `SCORE<br>${settings.score}`;

    moveRoad();
    moveEnemy();

    if (keys.ArrowLeft && settings.x > 0) {
      settings.x -= settings.speed;
    }

    if (keys.ArrowRight && settings.x < (gameArea.offsetWidth - car.offsetWidth)) {
      settings.x += settings.speed;
    }

    if (keys.ArrowDown && settings.y < (gameArea.offsetHeight - car.offsetHeight)) {
      settings.y += settings.speed;
    }

    if (keys.ArrowUp && settings.y > 0) {
      settings.y -= settings.speed;
    }

    car.style.left = settings.x + 'px';
    car.style.top = settings.y + 'px';

    requestAnimationFrame(playGame);
  }
}

/**
* Старт игры
*/
function startRun(event) {
  event.preventDefault();
  keys[event.key] = true;
}

/**
* Остановка игры
*/
function stopRun(event) {
  event.preventDefault();
  keys[event.key] = false;
}
